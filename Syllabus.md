### Syllabus

#### Lecturer

* Name: Dr. Thomas Köntges 
* Phone: 0341 - 97 32348
* Email: thomas.koentges@uni-leipzig.com
* Twitter: @ThomasKoentges

##### Office Hours

Tuesdays 10:00 - 12:00, Paulinum Raum P-618, on request

Online: GoogleHangout, thomas.koentges@gmail.com, on request

#### Class Resources

* GitLab: https://git.informatik.uni-leipzig.de/introduction-dh/
* OLAT: https://olat.informatik.uni-leipzig.de/

#### Expectations for Class Participation

* Physical presence in the lectures
* Preparation for and active participation in the seminars
* The assignments given in the seminars are due 48h before the next seminar
* Handing-in of the project report by March 01, 2017 (Prüfungsleistung)

#### Venues

* V: Lecture, Felix-Klein-Hörsaal P501 P5.014, Mondays 1.15-2.45pm
* S: Seminar,
	* Group A, Seminarraum S312 S 3.205, Tuesdays, 5.15-6.45pm
	* Group B, Seminarraum S312 S 3.205, Thursdays, 3.15-4.45pm
	* Group C, Rechnerlabor P801 P8.011, Wednesdays, 1.15-2.45pm
* P: Praktikum (Vorlesungsfreie Zeit)

#### Schedule

| Number | Form | Topic | When |
| ------ | ---- | ----- | ---- |
| 1 | V | Greg's Welcome / Housekeeping / Everything you wanted to know about DH but haven't yet asked | 10.10. |
| 2 | S | How to Git, Github & Gitlab / Stackoverflow | 11./12./13.10 |
| 3 | V | History of DH & Non-textual trends in DH | 17.10. |
| 4 | S | Introduction to APIs and Data formats / Data Cleaning with OpenRefine | 18./19./20.10 |
| 5 | V | Combining Qualitative and Quantitative Research Methods | 24.10. |
| 6 | S | More Data Cleaning & Basic Google Fusion Visualisations | 25./26./27.10 |
| 7 | V | Which language do you speak? Overview of programming languages used in Digital Humanities | 7.11. |
| 8 | S | Introduction to R for Digital Humanists (including Shiny & RMarkdown) | 8./9./10.11 |
| 9 | V | Introduction to Python for Digital Humanists (Matt Munson) | 14.11. |
| 10 | S | Introduction to Python for Digital Humanists (Matt Munson) | 15./16./17.11 |
| 11 | V | Natural Language Processing for Historical Languages (and other languages too) | 21.11. |
| 12 | S | Morphological Normalisation for Complex Languages - A how-to! | 22./23./24.11 |
| 13 | V | Topic Modelling & Word Vector Embedding | 28.11. |
| 14 | S | Topic Modelling & Word Vector Embedding with R | 29./30.11 & 1.12 |
| 15 | V | Challenges of Data Curation & Corpus Building | 5.12. |
| 16 | S | Introduction to Omeka / DublinCore / CTS/CITE-Architecture | 6./7./8.12 |
| 17 | V | Applications and Principles of DH | 12.12. |
| 18 | S | Plan your project | 13./14./15.12 |
| 19 | V | Citizen Science in the Humanities | 19.12. |
| 20 | P | Non-classroom project work | - |
| 21 | V | Discovering the history and transmission lines of information | 9.1. |
| 22 | S | R Repetitorium | 10./11./12.1 |
| 23 | V | Use Case: Mapping Arabic Literature (Maxim Romanov) | 16.1. |
| 24 | S | Fun with Maps in R (Maxim Romanov) | 17./18./19.1 |
| 25 | V | Analysis and Visualisation of Social Networks | 23.1. |
| 26 | S | Introduction to Gephi | 24./25./26.1 |
| 27 | V | Last Class: Repetitorium / Feedback | 30.1. |
| 28 | S | In-class project work | 31.1 & 1./2.2 |

